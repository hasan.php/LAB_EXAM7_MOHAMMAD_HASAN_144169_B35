<?php
namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;



class Email extends DB
{
    public $id;
    public $name;
    public $email;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function index()
    {
        echo "Im inside index method of Email!";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('email', $postVariableData)) {
            $this->email = $postVariableData['email'];
        }

    }//end of setData method

    public function store()
    {
        $arrData = array($this->name, $this->email);
        $sql = "Insert INTO email(name,email) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }
}
