<?php
namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Birthday extends DB
{
    public $id;
    public $name;
    public $b_date;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function index()
    {
        echo "Im inside index method of Birthday!";
    }

    public function setData($postVariableData= NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('b_date', $postVariableData)) {
            $this->b_date = $postVariableData['b_date'];
        }
    }


    public function store()
    {

        $arrData = array($this->name, $this->b_date);
        // var_dump($arrData);die;

        $sql = "INSERT INTO birthday(`name`, `b_date`) VALUES (?,?)";


        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);


        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');



    }

}


