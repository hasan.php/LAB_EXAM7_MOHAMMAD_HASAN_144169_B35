<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo "This is from BookTitle Class!";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('book_title', $postVariableData)) {
            $this->book_title = $postVariableData['book_title'];

        }
        if (array_key_exists('author_name', $postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
        }

    }//end of setData method

    public function store()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql = "Insert INTO booktitle(book_title,author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method



}//end of store method







