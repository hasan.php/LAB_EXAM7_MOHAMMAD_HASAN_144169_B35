<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;




class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobbies;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo "This is from Hobbies Class!";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('hobbies', $postVariableData)) {
            $this->hobbies = $postVariableData['hobbies'];
        }

    }//end of setData method

    public function store()
    {
        $arrData = array($this->hobbies, $this->name);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }
}
